import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GlassesEditPage } from './glasses-edit.page';

describe('GlassesEditPage', () => {
  let component: GlassesEditPage;
  let fixture: ComponentFixture<GlassesEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlassesEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GlassesEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
