import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GlassesEditPage } from './glasses-edit.page';

const routes: Routes = [
  {
    path: '',
    component: GlassesEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GlassesEditPageRoutingModule {}
