import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Glasses } from '../shared/glasses';
import { ActivatedRoute, Router } from '@angular/router';
import { GlassesService } from '../shared/glasses.service';

@Component({
  selector: 'app-glasses-edit',
  templateUrl: './glasses-edit.page.html',
  styleUrls: ['./glasses-edit.page.scss'],
})
export class GlassesEditPage implements OnInit {

  pageTitle: string;
  errorMessage: string;
  glassesForm: FormGroup;

  gId: number;
  glasses: Glasses;

  constructor(private fb: FormBuilder,
    private activatedroute: ActivatedRoute,
    private router: Router,
    private glassesService: GlassesService) { }

  ngOnInit() {
    this.glassesForm = this.fb.group({
      title: '',
      price: '',
      description: '',
      image: ''
    });
    this.gId = parseInt(this.activatedroute.snapshot.params['id']);
    this.getGlasses(this.gId);
  }

  getGlasses(id: number): void {
    this.glassesService.getGlassesById(id)
      .subscribe(
        (product: Glasses) => this.displayGlasses(product),
        (error: any) => this.errorMessage = <any>error
      );
  }

  displayGlasses(glasses: Glasses): void {
    if (this.glassesForm) {
      this.glassesForm.reset();
    }
    this.glasses = glasses;
    this.pageTitle = `Edit Product: ${this.glasses.title}`;

    // Update the data on the form
    this.glassesForm.patchValue({
      title: this.glasses.title,
      price: this.glasses.price,
      description: this.glasses.description,
      image: this.glasses.image
    });
  }


  saveGlasses(): void {
    if (this.glassesForm.valid) {
      if (this.glassesForm.dirty) {
        this.glasses = this.glassesForm.value;
        this.glasses.id = this.gId;
        
        this.glassesService.updateGlasses(this.glasses)
        .subscribe(
          () => this.onSaveComplete(),
          (error: any) => this.errorMessage = <any>error
        );
      
        
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.glassesForm.reset();
    this.router.navigate(['home']);
  }
}
