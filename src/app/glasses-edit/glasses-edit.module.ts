import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GlassesEditPageRoutingModule } from './glasses-edit-routing.module';

import { GlassesEditPage } from './glasses-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GlassesEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [GlassesEditPage]
})
export class GlassesEditPageModule {}
