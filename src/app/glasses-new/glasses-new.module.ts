import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GlassesNewPageRoutingModule } from './glasses-new-routing.module';

import { GlassesNewPage } from './glasses-new.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GlassesNewPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [GlassesNewPage]
})
export class GlassesNewPageModule {}
