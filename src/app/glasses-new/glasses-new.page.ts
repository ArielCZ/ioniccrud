import { Component, OnInit } from '@angular/core';
import { Glasses } from '../shared/glasses';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlassesService } from '../shared/glasses.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-glasses-new',
  templateUrl: './glasses-new.page.html',
  styleUrls: ['./glasses-new.page.scss'],
})
export class GlassesNewPage implements OnInit {

  errorMessage: string;
  glassesForm: FormGroup;

  gId: number;
  glasses: Glasses;
  
  constructor(private fb: FormBuilder,
    private router: Router,
    private activatedroute: ActivatedRoute,
    private glassesService: GlassesService,
    public toastController: ToastController
  ) { }



    ngOnInit(): void {
      this.glassesForm = this.fb.group({
        title: '',
        price: '',
        description: '',
        image: ''
      });
    this.gId = parseInt(this.activatedroute.snapshot.params['id']);
  }

  

  saveGlasses(): void {
    if (this.glassesForm.valid) {
      if (this.glassesForm.dirty) {
        this.glasses = this.glassesForm.value;
        this.glasses.id = this.gId;
        
        this.glassesService.createGlasses(this.glasses)
          .subscribe(
            () => this.onSaveComplete(),
            (error: any) => this.errorMessage = <any>error
          );
        
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.glassesForm.reset();
    this.router.navigate(['']);
  }

}
