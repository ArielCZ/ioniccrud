import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GlassesNewPage } from './glasses-new.page';

const routes: Routes = [
  {
    path: '',
    component: GlassesNewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GlassesNewPageRoutingModule {}
