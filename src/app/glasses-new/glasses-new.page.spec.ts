import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GlassesNewPage } from './glasses-new.page';

describe('GlassesNewPage', () => {
  let component: GlassesNewPage;
  let fixture: ComponentFixture<GlassesNewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlassesNewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GlassesNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
