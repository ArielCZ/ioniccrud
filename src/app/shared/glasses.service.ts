import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError} from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

import { Glasses } from './glasses';

@Injectable({
  providedIn: 'root'
})
export class GlassesService {
  private glassesUrl = 'api/glasses';

  constructor(private http: HttpClient) { }

  getGlasses(): Observable<Glasses[]> {
    return this.http.get<Glasses[]>(this.glassesUrl)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  getMaxGlassesId(): Observable<Glasses> {
    return this.http.get<Glasses[]>(this.glassesUrl)
    .pipe(
      // Get max value from an array
      map(data => Math.max.apply(Math, data.map(function(o) { return o.id; }))   ),
      catchError(this.handleError)
    );
  }

  getGlassesById(id: number): Observable<Glasses> {
    const url = `${this.glassesUrl}/${id}`;
    return this.http.get<Glasses>(url)
      .pipe(
        tap(data => console.log('getGlasses: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  createGlasses(g: Glasses): Observable<Glasses> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    g.id = null;
    return this.http.post<Glasses>(this.glassesUrl, g, { headers: headers })
      .pipe(
        tap(data => console.log('createGlasses: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  deleteGlasses(id: number): Observable<{}> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.glassesUrl}/${id}`;
    return this.http.delete<Glasses>(url, { headers: headers })
      .pipe(
        tap(data => console.log('deleteGlasses: ' + id)),
        catchError(this.handleError)
      );
  }

  updateGlasses(g: Glasses): Observable<Glasses> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.glassesUrl}/${g.id}`;
    return this.http.put<Glasses>(url, g, { headers: headers })
      .pipe(
        tap(() => console.log('updateProduct: ' + g.id)),
        // Return the product on an update
        map(() => g),
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }

}