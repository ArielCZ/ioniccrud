import { InMemoryDbService } from 'angular-in-memory-web-api';

export class GlassesData implements InMemoryDbService{
    createDb(){
        let glasses = [
            {
              "id": 0,
              "title": "First Glasses",
              "price": "40 €",
              "description": "Obsidian Blue",
              "image": "/assets/img/1.jpg"
            },
            {
                "id": 1,
                "title": "Second Glasses",
                "price": "35 €",
                "description": "Obsidian Red",
                "image": "/assets/img/2.jpg"
            },
            {
                "id": 2,
                "title": "Third Glasses",
                "price": "45 €",
                "description": "Obsidian Black",
                "image": "/assets/img/5.jpg"
            }         
          ];
          return { glasses: glasses };
    }
}