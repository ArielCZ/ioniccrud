export interface Glasses{
    id: number;
    title: string;
    price: number;
    description: string;
    image: '';
}