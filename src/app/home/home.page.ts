import { Component, OnInit, Input } from '@angular/core';
import { Glasses } from '../shared/glasses';
import { GlassesService } from '../shared/glasses.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  glasses: Glasses[] = [];
  constructor(private glassesService: GlassesService) { }

  ngOnInit() {
    this.glassesService.getGlasses().subscribe(
      (data: Glasses[]) => this.glasses = data
    );
  }

  ionViewWillEnter(){
    this.ngOnInit();
  }
}
