import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { GlassesItemComponent } from '../glasses-item/glasses-item.component';
import { GlassesService } from '../shared/glasses.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, GlassesItemComponent],
  providers: [GlassesService],
  entryComponents: []
})
export class HomePageModule {}
