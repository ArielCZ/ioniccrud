import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GlassesService } from './shared/glasses.service';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { GlassesData } from './shared/glasses-data';
import { HttpClientModule } from '@angular/common/http';
import { GlassesItemComponent } from './glasses-item/glasses-item.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    InMemoryWebApiModule.forRoot(GlassesData),
    HttpClientModule,
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GlassesService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
