import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'glasses-new/:id/new',
    loadChildren: () => import('./glasses-new/glasses-new.module').then( m => m.GlassesNewPageModule)
  },
  {
    path: 'glasses/:glassesId',
    loadChildren: () => import('./glasses-detail/glasses-detail.module').then( m => m.GlassesDetailPageModule)
  },
  {
    path: 'glasses-edit/:id/edit',
    loadChildren: () => import('./glasses-edit/glasses-edit.module').then( m => m.GlassesEditPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
