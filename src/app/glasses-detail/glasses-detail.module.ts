import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GlassesDetailPageRoutingModule } from './glasses-detail-routing.module';

import { GlassesDetailPage } from './glasses-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GlassesDetailPageRoutingModule
  ],
  declarations: [GlassesDetailPage]
})
export class GlassesDetailPageModule {}
