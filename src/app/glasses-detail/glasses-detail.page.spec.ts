import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GlassesDetailPage } from './glasses-detail.page';

describe('GlassesDetailPage', () => {
  let component: GlassesDetailPage;
  let fixture: ComponentFixture<GlassesDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlassesDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GlassesDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
