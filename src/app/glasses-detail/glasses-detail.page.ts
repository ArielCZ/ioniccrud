import { Component, OnInit, NgZone } from '@angular/core';
import { Glasses } from '../shared/glasses';
import { ActivatedRoute, Router } from '@angular/router';
import { GlassesService } from '../shared/glasses.service';
import { async } from '@angular/core/testing';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-glasses-detail',
  templateUrl: './glasses-detail.page.html',
  styleUrls: ['./glasses-detail.page.scss'],
})
export class GlassesDetailPage implements OnInit {

  glasses: Glasses;
  gId: number;
  hasValues: boolean = false;
  errorMessage: any;
  constructor(private activatedroute: ActivatedRoute,
     private router: Router,
      private glassesService: GlassesService,
    public loadingController: LoadingController,
    private ngZone: NgZone) { }

  ngOnInit() {
    this.gId = parseInt(this.activatedroute.snapshot.params['glassesId']);

    this.glassesService.getGlassesById(this.gId).subscribe(
      (data: Glasses) => {
        
        this.glasses = data;
        this.presentLoadingWithOptions();
        this.hasValues = true;
        
      }
    );

  }

  goEdit(): void {
    this.router.navigate(['/glasses-edit', this.gId, 'edit']);
  }
  onBack(): void {
    this.router.navigate(['']);
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: "circular",
      duration: 200,
      message: 'Cargando...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  deleteGlasses(): void {
    if (this.glasses.id < 0) {
      this.onSaveComplete();
    } else {
      if (confirm(`Really delete the product: ${this.glasses.title}?`)) {
        this.glassesService.deleteGlasses(this.glasses.id)
          .subscribe(
            () => this.onSaveComplete(),
            (error: any) => this.errorMessage = <any>error
          );
      }
    }
  }
  onSaveComplete() {
    this.ngZone.run(() =>{
      this.router.navigate(['home']);
    });
    
  }


}
