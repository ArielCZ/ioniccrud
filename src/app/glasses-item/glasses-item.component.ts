import { Component, OnInit, Input } from '@angular/core';
import { Glasses } from '../shared/glasses';

@Component({
  selector: 'app-glasses-item',
  templateUrl: './glasses-item.component.html',
  styleUrls: ['./glasses-item.component.scss'],
})
export class GlassesItemComponent {

  @Input() glasses: Glasses;

}
